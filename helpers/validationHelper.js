const validateEntity = (data) => {
    const {
        hasRequiredFields,
        validatedFields,
        entity,
        validationFunction,
        res,
        next,
        errorMessage,
    } = data;
    
    try {
        if (hasRequiredFields) {
            validatedFields.forEach(it => {
                const isFieldValid = validationFunction(entity, it);
                if (!isFieldValid) {
                    throw Error (errorMessage);
                }
            });
      
            next();
        } else {
            throw Error (errorMessage);
        }
    } catch (e) {
        res.status(400);
        res.json({error: true, message: e.message});
        res.send();
    }
}

const validateCreateEntity = (createData) => {
    const { req, modelEntity, ...data } = createData;
    const entity = req.body;
    const validatedFields = Object.keys(modelEntity).filter(it => it !== 'id').sort();
    const hasRequiredFields = JSON.stringify(validatedFields) === JSON.stringify(Object.keys(entity).sort());
    const errorMessage = 'Entity to create isn\'t valid';
    
    validateEntity({...data, entity, validatedFields, hasRequiredFields, errorMessage});
  
}

const validateUpdateEntity = (updateData) => {
    const { req, ...data } = updateData;
    const entity = req.body;
    const validatedFields = Object.keys(entity);
    const hasRequiredFields = !!validatedFields.length;
    const errorMessage = 'Data isn\'t valid for entity update';
    
    validateEntity({...data, entity, validatedFields, hasRequiredFields, errorMessage});
}

exports.validateCreateEntity = validateCreateEntity;
exports.validateUpdateEntity = validateUpdateEntity;