const { fight } = require('../models/fight');

const handleError = (response, errorMessage) => {
  response.status(400);
  response.json({error: true, message: errorMessage});
  response.send();
}

const createFightValid = (req, res, next) => {
  const entity = req.body;
  const validatedFields = Object.keys(fight).filter(it => it !== 'id').sort();
  const hasRequiredFields = JSON.stringify(validatedFields) === JSON.stringify(Object.keys(entity).sort());
  if (hasRequiredFields) {
    const logKeys = Object.keys(...entity.log).sort();
    const modelLogKeys = Object.keys(...fight.log).sort();
    const logHasRequiredFields = JSON.stringify(logKeys) === JSON.stringify(modelLogKeys);
    if (logHasRequiredFields) {
      next();
    } else {
      handleError(res, 'Entity to create isn\'t valid');
    }
  } else {
    handleError(res, 'Entity to create isn\'t valid');
  }
}

const updateFightValid = (req, res, next) => {
  const log = req.body;
  const logKeys = Object.keys(log).sort();
  const modelLogKeys = Object.keys(...fight.log).sort();
  const logHasRequiredFields = JSON.stringify(logKeys) === JSON.stringify(modelLogKeys);
  if (logHasRequiredFields) {
    next();
  } else {
    handleError(res, 'Data isn\'t valid for entity update');
  }
}

exports.createFightValid = createFightValid;
exports.updateFightValid = updateFightValid;