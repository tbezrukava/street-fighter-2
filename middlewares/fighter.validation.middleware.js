const { fighter } = require('../models/fighter');
const { validateCreateEntity, validateUpdateEntity } = require('../helpers/validationHelper');

const createFighterValid = (req, res, next) => {
    if (!req.body.health) {
        req.body.health = "100";
    }
    
    const data = {
        req,
        res,
        next,
        validationFunction: fieldValid,
        modelEntity: fighter
    };
    
    validateCreateEntity(data);
}

const updateFighterValid = (req, res, next) => {
    const data = {
        req,
        res,
        next,
        validationFunction: fieldValid,
    };
    
    validateUpdateEntity(data);
}

const nameValid = (name = '') => !!name.trim().length;
const healthValid = (health = '') => !isNaN(+health) && +health >= 80 && +health <= 120;
const powerValid = (power = '') => !isNaN(+power) && +power >= 1 && +power <= 100;
const defenseValid = (defense = '') => !isNaN(+defense) && +defense >= 1 && +defense <= 10;

function fieldValid(fighter, field) {
    const { name, health, power, defense } = fighter;
    let isValid;
    
    switch (field) {
        case 'name':
            isValid = nameValid(name);
            break;
        case 'health':
            isValid = healthValid(health);
            break;
        case 'power':
            isValid = powerValid(power);
            break;
        case 'defense':
            isValid = defenseValid(defense);
            break;
        default:
            isValid = false;
    }
    
    return isValid;
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;