const { Router } = require('express');
const FightService = require('../services/fightService');

const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFightValid, updateFightValid } = require('../middlewares/fight.validation.middleware');

const router = Router();

router.get('/', function(req, res, next) {
  try {
    res.data = FightService.getAllFights();
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.get('/:id', function(req, res, next) {
  try {
    const fightId = req.params.id;
    res.data = FightService.getFight(fightId);
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.post('/', createFightValid, function(req, res, next) {
  try {
    res.data = FightService.createFight(req.body);
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.put('/:id', updateFightValid, function(req, res, next) {
  try {
    const fightId = req.params.id;
    const dataToUpdate = req.body;
    res.data = FightService.updateFight(fightId, dataToUpdate);
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.delete('/:id', function(req, res, next) {
  const fightId = req.params.id;
  res.data = FightService.deleteFight(fightId);
  next();
}, responseMiddleware);

module.exports = router;