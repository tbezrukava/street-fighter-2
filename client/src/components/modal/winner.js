import { showModal } from './modal';
import { createElement } from '../../services/domHelper';

export function showWinnerModal(fighter) {
  const title = `The Game Is Over`;
  const bodyElement = createElement({tagName: 'div', className: 'modal-body'});
  bodyElement.innerHTML = `${fighter.name} has won`
  const onClose = () => {
    const rootElement = document.getElementById('root');
    rootElement.innerHTML = '';
  };
  showModal({title, bodyElement, onClose});
}
