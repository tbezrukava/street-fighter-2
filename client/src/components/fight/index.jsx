import React from 'react';

import { post } from '../../services/requestHelper';
import { getFighters } from '../../services/domainRequest/fightersRequest';
import { renderArena } from '../arena';
import NewFighter from '../newFighter';
import Fighter from '../fighter';
import { Button } from '@material-ui/core';

import './fight.css'

class Fight extends React.Component {
    state = {
        fighters: [],
        fighter1: null,
        fighter2: null
    };

    async componentDidMount() {
        const fighters = await getFighters();
        if(fighters && !fighters.error) {
            this.setState({ fighters });
        }
    }

    onFightStart = () => {
        const { fighter1, fighter2 } = this.state;
        if (fighter1 && fighter2) {
            const body = {
                fighter1: fighter1.id,
                fighter2: fighter2.id,
                log: [
                    {
                        fighter1Shot: "0",
                        fighter2Shot: "0",
                        fighter1Health: fighter1.health,
                        fighter2Health: fighter2.health
                    }
                ]
            }
            
            post('fights', body)
                .then((data) => renderArena(data.id, [
                    { ...fighter1, source: "https://media.giphy.com/media/kdHa4JvihB2gM/giphy.gif" },
                    { ...fighter2, source: "https://i.pinimg.com/originals/46/4b/36/464b36a7aecd988e3c51e56a823dbedc.gif" }
                ]));
        }
    }

    onCreate = (fighter) => {
        this.setState({ fighters: [...this.state.fighters, fighter] });
    }

    onFighter1Select = (fighter1) => {
        this.setState({ fighter1 });
    }

    onFighter2Select = (fighter2) => {
        this.setState({ fighter2 });
    }

    getFighter1List = () => {
        const { fighter2, fighters } = this.state;
        if(!fighter2) {
            return fighters;
        }

        return fighters.filter(it => it.id !== fighter2.id);
    }

    getFighter2List = () => {
        const { fighter1, fighters } = this.state;
        if(!fighter1) {
            return fighters;
        }

        return fighters.filter(it => it.id !== fighter1.id);
    }

    render() {
        const  { fighter1, fighter2 } = this.state;
        return (
            <div id="wrapper">
                <NewFighter onCreated={this.onCreate} />
                <div id="figh-wrapper">
                    <Fighter selectedFighter={fighter1} onFighterSelect={this.onFighter1Select} fightersList={this.getFighter1List() || []} />
                    <div className="btn-wrapper">
                        <Button onClick={this.onFightStart} variant="contained" color="primary">Start Fight</Button>
                    </div>
                    <Fighter selectedFighter={fighter2} onFighterSelect={this.onFighter2Select} fightersList={this.getFighter2List() || []} />
                </div>
            </div>
        );
    }
}

export default Fight;