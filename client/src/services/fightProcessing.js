import { controls } from '../constants/controls';
import { put } from './requestHelper';

export async function fightProcessing(fightId, playerOne, playerTwo) {
  return new Promise((resolve) => {
    let gameOver = false;
    
    const leftFighter = {
      health: playerOne.health,
      attack: playerOne.power,
      defense: playerOne.defense,
      currentHealth: playerOne.health,
      isBlocked: false,
      criticalHitPermission: true,
      healthIndicator: document.getElementById('left-fighter-indicator')
    };
    
    const rightFighter = {
      health: playerTwo.health,
      attack: playerTwo.power,
      defense: playerTwo.defense,
      currentHealth: playerTwo.health,
      isBlocked: false,
      criticalHitPermission: true,
      healthIndicator: document.getElementById('right-fighter-indicator')
    }
    
    runBlock(controls.PlayerOneBlock, leftFighter);
    runBlock(controls.PlayerTwoBlock, rightFighter);
    runAttack(controls.PlayerOneAttack, leftFighter);
    runAttack(controls.PlayerTwoAttack, rightFighter);
    runCriticalHit(controls.PlayerOneCriticalHitCombination, leftFighter);
    runCriticalHit(controls.PlayerTwoCriticalHitCombination, rightFighter);
    
    function runAttack(attackKey, fighter) {
      document.addEventListener('keyup', function(e) {
        if (gameOver) {
          return
        }
        
        if (!fighter.isBlocked) {
          if (e.code === attackKey) {
            const attacker = fighter === leftFighter ? leftFighter : rightFighter;
            const defender = fighter === leftFighter ? rightFighter : leftFighter;
            if (!defender.isBlocked) {
              const shot = getDamage(attacker, defender)
              defender.currentHealth -= shot;
              updateFight(attacker, shot);
              changeIndicator(defender);
              checkGameOver(defender);
            }
          }
        }
      });
    }
    
    function runBlock(blockKey, fighter) {
      document.addEventListener('keydown', function(e) {
        if (e.code === blockKey) {
          fighter.isBlocked = true;
        }
      });
  
      document.addEventListener('keyup', function(e) {
        if (e.code === blockKey) {
          fighter.isBlocked = false;
        }
      });
    }
    
    function runCriticalHit(codes, fighter) {
      const pressedCriticalKeys = new Set();
      
      document.addEventListener('keydown', function(e) {
        if (gameOver) {
          return
        }
        
        if (codes.includes(e.code)) {
          pressedCriticalKeys.add(e.code);
          const allCriticalKeysPressed = pressedCriticalKeys.size === codes.length;
          
          if (allCriticalKeysPressed) {
            const attacker = fighter === leftFighter ? leftFighter : rightFighter;
            const defender = fighter === leftFighter ? rightFighter : leftFighter;
            
            if (attacker.criticalHitPermission && !attacker.isBlocked) {
              attacker.criticalHitPermission = false;
              setTimeout(() => attacker.criticalHitPermission = true, 10000);
              const shot = attacker.attack * 2;
              defender.currentHealth -= shot;
              updateFight(attacker, shot);
              changeIndicator(defender);
              checkGameOver(defender);
            }
          }
        }
      });
      
      document.addEventListener('keyup', function(e) {
        if (codes.includes(e.code)) {
          pressedCriticalKeys.delete(e.code);
        }
      });
    }
    
    function changeIndicator(defender) {
      if (defender.currentHealth <= 0) {
        defender.healthIndicator.style.width = '0';
      } else {
        defender.healthIndicator.style.width = `${Math.floor(100 / defender.health * defender.currentHealth)}%`
      }
    }
    
    function checkGameOver(defender) {
      if (defender.currentHealth <= 0) {
        gameOver = true;
        const winner = defender === leftFighter ? playerTwo : playerOne;
        resolve(winner);
      }
    }
  
    function updateFight(attacker, shot) {
      const body = {
        fighter1Shot: attacker === leftFighter ? shot.toString() : "0",
        fighter2Shot: attacker === rightFighter ? shot.toString() : "0",
        fighter1Health: leftFighter.currentHealth.toString(),
        fighter2Health: rightFighter.currentHealth.toString()
      };
    
      put('fights', fightId, body);
    }
  });
}

export function getDamage(attacker, defender) {
  const hitPower = getHitPower(attacker);
  const blockPower = getBlockPower(defender);
  return Math.max(0, hitPower - blockPower);
}

export function getHitPower(fighter) {
  return fighter.attack * random(1, 2);
}

export function getBlockPower(fighter) {
  return fighter.defense * random(1, 2);
}

function random(min, max) {
  return min + Math.random() * (max - min);
}