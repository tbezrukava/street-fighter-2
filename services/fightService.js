const { FightRepository } = require('../repositories/fightRepository');

class FightService {
    search(search) {
        const item = FightRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
    
    getFight(fightId) {
        const fight = this.search(it => it.id === fightId);
        if(!fight) {
            throw Error('Fight not found');
        }
        return fight;
    }
    
    getAllFights() {
      const fights = FightRepository.getAll();
      if(!fights) {
        throw Error('Request error');
      }
      return fights;
    }
    
    createFight(data) {
      return FightRepository.create(data);
    }
    
    updateFight(fightId, data) {
      const fight = this.getFight(fightId);
      const dataToUpdate = {
        log: [...fight.log, data]
      };
      return FightRepository.update(fightId, dataToUpdate)
    }
    
    deleteFight(fightId) {
      FightRepository.delete(fightId);
      return {};
    }
}

module.exports = new FightService();